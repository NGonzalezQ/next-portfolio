import { withRouter } from 'next/router'
import Layout from '../components/Layout'

const Post = ({ router }) => (
  <Layout title={router.query.title}>
    <p>
      Lorem ipsum text
    </p>
  </Layout>
)

export default withRouter(Post)
